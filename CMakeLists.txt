cmake_minimum_required( VERSION 2.6 )

project(multigrid_playground)

add_definitions(-Wall)


add_subdirectory(poisson_finite_difference)
add_subdirectory(poisson_finite_element)
