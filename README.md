# Multigrid Solver Playground

Some code for playing around with the multigrid method for solving PDEs. 

* poisson_finite_difference/  
C code that solved the Poisson equation in a rectangle. The discretization is the 
usual cell-centered finite difference method involving the immediate four neighbours.
* poisson_finite_element/    
This is slightly fancier. It has a half edge data structure, and functionality for
 non-nested grid intepolation. I am working on it.

 
