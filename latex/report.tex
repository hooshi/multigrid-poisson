%% -------------------------------------------------------
% Preamable ---------------------------------------------
%--------------------------------------------------------
\documentclass[letter,12pt]{article}
%-------------------------------------------------------
%\usepackage [colorlinks, linkcolor=blue,
%citecolor=magenta, urlcolor=cyan]{hyperref}   % pdf links
\usepackage {hyperref}
\usepackage{graphicx} % inserting image
\usepackage{setspace} % double/single spacing
\usepackage[top=2cm,right=2cm,bottom=2cm,left=2cm]{geometry} % margins 
\usepackage{amsmath}  %math formulas
\usepackage{array}    %beautiful tables
\usepackage[export]{adjustbox} %aligning images
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{color,soul} %highlight

\graphicspath{{images/}}

\newenvironment{tight_itemize}{
\begin{itemize}
  \setlength{\itemsep}{0pt}
  \setlength{\parskip}{0pt}
}{\end{itemize}}


% -------------------------------------------------------
% Author and title---------------------------------------
%--------------------------------------------------------
\title{First Programming Assignment of MECH510}
\date{October 2015}
\author{Shayan Hoshyari\\ Student \#: 81382153}

% -------------------------------------------------------
% Begin document-----------------------------------------
%--------------------------------------------------------
\begin{document}
\pagenumbering{arabic}
\maketitle

% -------------------------------------------------------
% Tables--------------------------------------------------
%--------------------------------------------------------
\tableofcontents

% -------------------------------------------------------
% ch1-Problem--------------------------------------------
%--------------------------------------------------------
\section{The Problems}
\label{ch:problem}
In this assignment the Poisson equation, Equation \eqref{eq:poisson},
has to be solved in the square $[0\quad1] \times [0\quad1]$, using a second
order finite volume method. Both the Neumann, Equation \eqref{eq:neu}, and 
Dirichlet, Equation \eqref{eq:dirich}, boundary conditions
have to be considered. For convenience the boundaries $y=0$, $x=1$, $y=1$, and 
$x=0$ will be called bnd1, bnd2, bnd3 and bnd4 respectively.

\begin{align}
\label{eq:poisson}
&T_{xx}+T_{yy}=f(x,y) \\
\label{eq:neu}
&T_n(z)= N(z) \quad z=x,y \quad \text{on the boundary} \\
\label{eq:dirich}
&T(z) = D(z) \quad z=x,y \quad \text{on the boundary} 
\end{align}

Two specific problems will be studied. Problem one has no source term and 
has an analytical solution in the form of Equation \eqref{eq:problem1}. The 
boundary conditions for this problem are $T=0$, $T_x=0$, $T=\cos(\pi x)$, and
$T_x=0$ for bnd1, bnd2, bnd3 and bnd4 respectively.
\begin{equation}
  \label{eq:problem1}
  T(x,y) = \frac{\cos(\pi x) \sinh(\pi y)}{\sinh \pi}
\end{equation}

In the second problem we use $P$ as the unknown instead of $T$ in accordance
with the physics of the problem. This problem is more complicated and has a source
term
which is defined as an operator over a specified velocity field. The original 
definition of the source term and its simplified form are shown in Equation
\eqref{eq:problem2}. The boundary conditions for this problem are $P_y=0$, 
$P=5-\frac{1}{2}(1+y^2)^3$, $P=5-\frac{1}{2}(1+x^2)^3$ and $P_x=0$ for bnd1,
bnd2, bnd3 and bnd4 respectively.

\begin{equation}
  \label{eq:problem2}
  \begin{aligned}
    &u(x,y) = x^3 - 3x y^2 \\
    &v(x,y) = -3x^2 y + y^3 \\
    &f(x,y) =   -(u_x^2 + 2v_x u_y + v_y^2)  =  -18(x^2+y^2)^2
  \end{aligned}
\end{equation}

% -------------------------------------------------------
% ch2-Numerical Method-----------------------------------
%--------------------------------------------------------
\section{Numerical Method}
\label{ch:numerical}
In this section the general solution procedure and the key formulas will be
presented. The general numerical algorithm is given in Algorithm \ref{alg:all}. 
%%algorithm of our method
\begin{algorithm}      
  \small
  \caption{Numerical Solution of the Poisson equation on a unit square}  
  \label{alg:all}                 
  \begin{algorithmic}[1]	
    \REQUIRE $NI$, $NJ$, $\omega$, $\epsilon$
    \ENSURE $\bar{T}_{i,j}$, $\| E \|_1$,  $\| E \|_2$, $\| E \|_\infty$
    \STATE $\Delta x = \frac{1}{NI}$, $\Delta y = \frac{1}{NJ}$
    \STATE Find the coordinates of the center of control volumes: 
    $x_{ij}, y_{ij}$
    \STATE Find the value of source term at the center of CV's: $f_{ij}$
    \STATE $c = 0$
    \WHILE {$n_{it} < 100000$}
    \STATE Find the value of all ghost cells according to Equation \eqref{eq:ghost}
    \STATE Find the new value of all cells according to Equation \eqref{eq:sor} 
and  $d_{max} = \max\left(T_{new} - T_{old}\right)$ 
    \STATE Print $n_{it}$ and $d_{max}$.
    \STATE $n_{it}=n_{it}+1$; 
    \IF {$d_{max} < \epsilon$}
    \STATE $c=1$
    \STATE Break.
    \ENDIF
    \ENDWHILE
    \IF{$c=1$}
    \STATE State success.
    \STATE Calculate the exact mean value of solution at each CV using 
    Equations \eqref{eq:q1} and \eqref{eq:q2}.
    \STATE Calculate error at each CV.
    \STATE Calculate the Error $L$ norms using the errors evaluated at each CV.
    \STATE Print the number of iterations and $L$ norms.
    \STATE Output the numerical value of solution and error at each CV in vtk
    format.
    \ELSE
    \STATE State failure.
    \ENDIF
  \end{algorithmic}      
\end{algorithm}


In this algorithm we have used 
the SOR method to solve our system of linear equations, which itself is derived
using a finite volume method. Equation \eqref{eq:sor} shows the derived SOR 
iteration formula. In this equation D is a dimensionless parameter defined
as $(\Delta y/ \Delta x)^2$.
%% Equation of SOR
\begin{equation}
\label{eq:sor}
  \begin{aligned}
    &T^*_{ij} = \frac{1}{2(D+1)} \left[
      (T_{i+1,j}+T_{i-1,j})D + T_{i,j+1}+T_{i-1,j-1} - f_{ij}\Delta y ^2
    \right] \\
    & \delta_{ij} = T^*_{ij} - T_{ij} \\
    & T_{ij} := T_{ij} + \omega \delta_{ij}
  \end{aligned}
\end{equation}
% 
In addition, ghost cells are used to impose the boundary
conditions. The solution value at a ghost cell is evaluated using Equation 
\eqref{eq:ghost}.
% Ghost cell equations
\begin{equation}
  \label{eq:ghost}
  \begin{aligned}
    & T_{gD}=2D_s - T_s \\
    & T_{gN}= T_s + \alpha  N_s \Delta z
  \end{aligned}
\end{equation}
% Ghost cell itemize
\begin{tight_itemize}
\item[$D_s$] Value of $T$ that should be enforced at the ghost cell's adjacent 
  wall.
\item[$N_s$] Value of $\frac{\partial T}{\partial n} $ that should be enforced 
  at the ghost cell's adjacent wall.
\item[$T_{gD}$] Value of a ghost cell at a Dirichlet boundary.
\item[$T_{gN}$] Value of a ghost cell at a Neumann boundary.
\item[$\Delta z$] Means $\Delta x$ for bnd2 and bnd4, and means 
  $\Delta y$ for bnd1 and bnd3.
\item[$\alpha$] Is equal to $+1$ for bnd2 and bnd3 and is equal to $-1$ for bnd4
  and bnd1.   
\end{tight_itemize}
% 

To find the error $L$ norms of the solution, the average value of the analytical
solution at each control volume is used. While using the analytical solution 
value at the 
midpoint of the CV would have been sufficient, we have used the 9 point Gauss 
Quadrature scheme to evaluate the analytic average values. This method is 
presented in Equations \eqref{eq:q1} and \eqref{eq:q2} for a 
function called $g(x,y)$ in a rectangle in the form 
$[a_1 \quad a_2] \times [b_1 \quad b_2]$.
%% Equation of gauss quadrature
\begin{align}
  \label{eq:q1}
  &\begin{array}{ l l }
    x_1 =  \frac{1+\sqrt{3}/5}{2} a_1 +  \frac{1-\sqrt{3}/5}{2} a_2
    &y_1 =  \frac{1+\sqrt{3}/5}{2} b_1 +  \frac{1-\sqrt{3}/5}{2} b_2 \\
    x_2 =  \frac{a_1+ a_2}{2}
    &y_2 =  \frac{b_1+ b_2}{2} \\
    x_3 =  \frac{1-\sqrt{3}/5}{2} a_1 +  \frac{1+\sqrt{3}/5}{2} a_2
    &y_3 =  \frac{1-\sqrt{3}/5}{2} b_1 +  \frac{1+\sqrt{3}/5}{2} b_2  \\    
  \end{array} \\
  % 
  \label{eq:q2}
  &\begin{aligned}
    \bar{g} = \frac{1}{4} 
    & \left\{ \frac{8}{9} \cdot \frac{8}{9} g(x_2,y_2) +  \right. \\
    &\frac{8}{9} \cdot \frac{1}{9} \left[ g(x_2,y_1)+g(x_2,y_3)+g(x_1,y_2)+g(x_3,y_2) \right] + \\  
    &\left.  \frac{1}{9} \cdot \frac{1}{9}  \left[ g(x_1,y_1)+g(x_3,y_3)+g(x_1,y_3)+g(x_3,y_1) \right] \right\}
  \end{aligned}
\end{align}


 % -------------------------------------------------------
 % ch3-Results--------------------------------------------
 %--------------------------------------------------------
\section{Results}
\label{ch:result}
In this section the solutions to each parts of the assignment are presented.

\subsection{Point Gauss-Sidel and Point SOR}
Problem one is solved on a $10\times10$ structured mesh,
with $\omega=1,1.5$ and $\epsilon=10^{-7}$.
The resulting values of errors at each control volume
is almost identical in both cases and is presented in Figure \ref{fig:10x10}.
Table \ref{tab:10x10}
shows the error norms and the number of iterations for each case.
As we expected, using the value of $1.5$ for $\omega$ reduces the number of
iterations drastically.

\begin{figure}
  %% Figure for problem one
  \centering
  \includegraphics[width=.7\textwidth]{10x10error.png}
  \caption[The distribution of solution error in problem one]{The value of error at
    each control volum in problem one, using a $10\times10$ mesh, $\omega = 1$ 
    and $\epsilon=10^{-7}$.}
  \label{fig:10x10}
  %% some space
  \vspace{.75cm}
  %% Figure for problem 2
  \includegraphics[width=.7\textwidth]{w-performance.eps}
  \caption[Effect of $\omega$ on convergence]{Maximum change in the solution
    per timestep for three different values of $\omega$ on  a $20\times20$ mesh.}
  \label{fig:20x20}
\end{figure}

\begin{table}
  \centering
  \caption{The error $L$ norms and number of iterations for problem one 
    on a $10\times10$ mesh}
  \label{tab:10x10}
  \begin{tabular}{c c c c c}
    \hline
    $\omega$ &$L_1$ &$L_2$ &$L_{\infty}$ &number of iterations\\
    \hline
    1 &$1.2499\times10^{-3}$ &$2.4570\times10^{-3}$ &$9.0954\times10^{-3}$ &$209$  \\
    1.5 &$1.2497\times10^{-3}$ &$2.4570\times10^{-3}$ &$9.0956\times10^{-3}$ &$92$ \\
    \hline
  \end{tabular}
\end{table}

\subsection{Convergence Behaviour}
We will solve problem one on a $20\times20$ mesh for three 
different
values of $\omega$, i.e., $1$, $1.3$ and $1.5$, while $\epsilon=10^{-7}$.
Then we will plot the maximum change in the solution per iteration in each case,
in an effort to show how $\omega$ affects the solution convergence of 
our linear system 
of equations. This plot is shown in Figure \ref{fig:20x20} and  clearly 
shows that, as the value of $\omega$ increases from $1$ to $1.5$ the maximum 
change in solution drops under $\epsilon$ faster, verifying the positive effect
of Successive Over Relaxation.

\subsection{Accuracy}
The accuracy of the numerical method will be analyzed, by studying 
the behaviour of error $L$ norms as the mesh gets smaller. Note that for a
p-th order method we expect that:
\begin{equation}
  \label{eq:acc1}
  \| E \| = C \left( \Delta x \right) ^ {p}
\end{equation}
Taking the logarithm of both sides of Equation \eqref{eq:acc1} and using 
the fact that
$\Delta x = \frac{1}{N}$ (N is the number of control volumes in each
direction), we will have:
\begin{equation}
  \label{eq:acc2}
  log \| E \| = -p \log N  - \log C
\end{equation}
Equation \eqref{eq:acc2} means that p will be the slope of the 
$\log \| E \| - log N$ diagram.

To study the order of accuracy our method problem one is solved on meshes with 
dimensions: $10\times10$, $20\times20$, $40\times40$, $80\times80$, and 
$160\times160$ and the resulting
norm vs. mesh size diagrams are shown in Figure \ref{fig:acc}. 
The linearity of these
diagrams are in agreement with our expectation of the behaviour of structured
meshes. The slope of the diagrams are found using linear least square curve fitting
and is presented in Table \ref{tab:acc}. Being close to 2, these slopes verify 
that our method is truly second order for problem one.

%% Figure for problem 4
\begin{figure}
  \centering
  \includegraphics[width=.7\textwidth]{n-performance.eps}
  \caption{The norm of error vs. number of cells in each direction.}
  \label{fig:acc}
\end{figure}

%% Table for problem 4
\begin{table}
  \centering
  \caption{Slope of $\log \| E \| - N$ diagrams}
  \label{tab:acc}
  \begin{tabular}{c c c}
    \hline
    $L_1$ &$L_2$ &$L_{\infty}$ \\
    \hline
    $1.9652$  &$1.9812$ &$1.9013$ \\
    \hline
  \end{tabular}
\end{table}

\subsection{Pressure Problem}

Problem two is solved by adding a source term to our laplace solver, as shown in 
Equation \eqref{eq:sor}. In Figure \ref{fig:p2} the 
average values of control volumes are shown on a $320\times320$ mesh. All the
results obtained in this section are obtained by setting \hl{$\epsilon=10^{-10}$} and
$\omega=1.5$.

%% Figure for problem pressure
\begin{figure}
  \centering
  \includegraphics[width=.7\textwidth]{320x320p.png}
  \caption{Solution for problem two on a $320\times320$ mesh}
  \label{fig:p2}
\end{figure}


%% Figure for control volume labeling
\begin{figure}
  \centering
  \includegraphics[width=.25\textwidth]{label.pdf}
  \caption{Labeling control volumes containing the point $^1/_2,^1/_2$}
  \label{fig:p2label}
\end{figure}

To evaluate the pressure at the point $e=\left(^1/_2,^1/_2\right)$ we 
interpolate the average 
values of the surrounding control volumes to get a second order approximation. 
First we label the surrounding control volumes according to Figure 
\ref{fig:p2label}.
Then using a taylor series expansion around $e$, we can show that (all the
derivatives are evaluated at $e$):
%% CV averages
\begin{align}
  \label{eq:ru}
  \bar{P}_{ru}=P_e +P_x\frac{\Delta x}{2} +P_y\frac{\Delta y}{2}
  +P_{xx}\frac{\Delta x ^ 2}{6} 
  +P_{xy}\frac{\Delta x \Delta y }{4}
  +P_{yy}\frac{\Delta y ^ 2}{6} + \cdots \\
  \label{eq:rd}
  \bar{P}_{rd}=P_e +P_x\frac{\Delta x}{2} -P_y\frac{\Delta y}{2}
  +P_{xx}\frac{\Delta x ^ 2}{6} 
  -P_{xy}\frac{\Delta x \Delta y }{4}
  +P_{yy}\frac{\Delta y ^ 2}{6} + \cdots \\
  \label{eq:lu}
  \bar{P}_{lu}=P_e -P_x\frac{\Delta x}{2} +P_y\frac{\Delta y}{2}
  +P_{xx}\frac{\Delta x ^ 2}{6} 
  +P_{xy}\frac{\Delta x \Delta y }{4}
  +P_{yy}\frac{\Delta y ^ 2}{6} + \cdots \\
  \label{eq:ld}
  \bar{P}_{ld}=P_e -P_x\frac{\Delta x}{2} -P_y\frac{\Delta y}{2}
  +P_{xx}\frac{\Delta x ^ 2}{6} 
  -P_{xy}\frac{\Delta x \Delta y }{4}
  +P_{yy}\frac{\Delta y ^ 2}{6} + \cdots 
\end{align}
Now using Equations \eqref{eq:ru}-\eqref{eq:ld} we can easily show that 
the average of average pressure values(!) at the surrounding control volumes
is a second order approximation for $P_e$, i.e.:
%% The interpolation
\begin{equation}
  \label{eq:interp}
  P_e = \frac{1}{4} \left( \bar{P}_{ru} + \bar{P}_{rd} + \bar{P}_{lu} 
    + \bar{P}_{ld} \right) 
  + O(\Delta x ^ 2,\Delta y ^2)
\end{equation}

For a sequence of $40\times40$, $80\times80$, and $160\times160$ meshes the 
values of surrounding control volumes and the interpolated value for $P_e$ 
are shown
in Table \ref{tab:p2}. Now using Richardson Extrapolation we will find  
approximations to the order of accuracy and error bound of our solution.
For the order of accuracy we can write:
%% The interpolation
\begin{equation}
  \label{eq:orderofconvergence}
  p = \log_2 \frac {
    \left| P|_{M1} - P|_{M2} \right|
  }{
    \left| P|_{M2} - P|_{M3} \right|
  } = 1.995
\end{equation}
%The extrapolation
Equation \eqref{eq:orderofconvergence} approximates the solution's order of 
accuracy to be \hl{$1.995$}. Now based on the ASME solution accuracy handout we 
extrapolate a more accurate value for $P_e$.
%% Error bound
\begin{equation}
  \label{eq:pext}
  P_{\text{ext}} =  \frac{2^p \times P|_{M3} - P|_{M2}}{2^p-1} =  4.937499725036921
\end{equation}
%The error bounds
Now we can calculate the error bound for our solution by using either the
extrapolated pressure value or the fine mesh pressure value:
%% Error bound
\begin{align}
  \label{eq:ebound}
  &e_a^{M3M2}  = \frac{P|_{M3} - P|_{M2}}{P_{M3}} \times 100 = :0.00094 \% \\
  &e_{\text{ext}}^{M3M2}  = \frac{P_{\text{ext}} - P|_{M3}}{P_{\text{ext}}} \times 100 
  = 0.00031 \%
\end{align}
Hopefully, we see that the final error in the pressure at the midpoint is only
\hl{$0.00031 \%$}.
% p:1.671719102553274
% p21:4.937471416382905
% e21:0.001198207069920
% e21x:0.000548145709118

%% Table for T_e
\begin{table}
  \centering
  \caption{Values of $P_e$ on a series of refined meshes}
  \label{tab:p2}
  \begin{tabular}{c c c c c c}
    \hline
   Size &$\bar{P}_{lu}$ &$\bar{P}_{ru}$ &$\bar{P}_{ld}$ &$\bar{P}_{rd}$ &$P_{e}$  \\
    \hline
    $40\times40$
   &$4.9379814815$    &$4.9281345750$ 
   &$4.9468909663$    &$4.9379814815$ 
   &$4.9377471261$     \\
   $80\times80$
   &$4.9376203616$    &$4.9328152779$ 
   &$4.9421910752$    &$4.9376203616$ 
   &$4.9375617691$    \\
   $160\times160$
   &$4.9375299330$    &$4.9351568380$
   &$4.9398444345$   &$4.9375299330$
   &$4.9375152846$   \\ 
    \hline
  \end{tabular}
\end{table}

% -------------------------------------------------------
% ch4-Conclusion-----------------------------------------
%--------------------------------------------------------
\section{Conclusion}
\label{ch:conclusion}
In this assignment the Poisson equation was solved in a unit square with Dirichlet
and  Neumann boundary conditions. A cell centered finite volume method was 
implemented on different structured grids to discretize the PDE and SOR and 
Gauss-Sidel methods were used to solve the resulting system of algebraic equations.

In addition the order of accuaracy of the numerical method was studied using 
both the method of manufactured solutions and the Richardson Extrapolation Method.
It was shown that the implemented method is second order accurate.

\end{document}