/** laplace.exe.c
 *  contains the main file that solves the laplace problem.
 *  Shayan Hoshyari
 *  October 2015
 */

#include <math.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

#include "defs.h"


static double func_cos_px(const double y, void* ctx)
{
  return cos(PI*y);
} 
static double func_zero(const double y, void* ctx)
{
  return 0;
} 
static double func_problem_2(const double y, void* ctx)
{
  const double z = (1+y*y);
  return 5-.5*z*z*z;
} 

//analytical solution
// static double _fanalytic(const double x,const double y, void* data)
// {
//   return cos(PI*x)*sinh(PI*y)/sinh(PI);
// }

//source term
static double problem_1_source(const double x, const double y, void* data)
{
  return 0;
}
static double problem_2_source(const double x, const double y, void* data)
{
  const double r = x*x+y*y;
  return -18 * r*r;
}

static struct Physics Physics_create_default(const int pid)
{
  struct Physics physics;
  
  // Create the physics
  if(pid == 0)
    {
      physics.boundary_south.type = BDRY_DIRICHLET;
      physics.boundary_east.type =  BDRY_NEUMANN;
      physics.boundary_north.type = BDRY_DIRICHLET;
      physics.boundary_west.type =  BDRY_DIRICHLET;
      physics.boundary_south.value = func_zero;
      physics.boundary_east.value =  func_zero;
      physics.boundary_north.value = func_cos_px;
      physics.boundary_west.value =  func_cos_px;
      physics.source_term = problem_1_source;
    }
  else
    {
      physics.boundary_south.type = BDRY_NEUMANN;
      physics.boundary_east.type =  BDRY_DIRICHLET;
      physics.boundary_north.type = BDRY_DIRICHLET;
      physics.boundary_west.type =  BDRY_NEUMANN;
      physics.boundary_south.value = func_zero;
      physics.boundary_east.value =  func_problem_2;
      physics.boundary_north.value = func_problem_2;
      physics.boundary_west.value =  func_zero;
      physics.source_term = problem_2_source;
    }

  return physics;
}

//
// Test smoothing, and transfer, don't do cycle
// 
int main(int argc, char *argv[])
{
  struct Grid *grid;
  struct Physics physics;
  struct MGInfo mg_info;
  struct MG *mg;
  int  i;

  // Create the Grid
  Grid_create(&grid, 1, 1, 1024, 1024);

  // physics
  physics = Physics_create_default(1);
 
  // Set the multigrid options
  mg_info = MGInfo_create(SMOOTHER_JACOBI, CYCLE_TYPE_V, 8);

  mg_info.n_levels = 10;
  mg_info.smoother_type = SMOOTHER_GAUSS_SEIDEL;
  mg_info.cycle_type = CYCLE_TYPE_V;
  mg_info.relaxation_factor = 2/3.;
  mg_info.atol = 1e-10;
  mg_info.n_smoothing[0] = 2;
  mg_info.n_smoothing[1] = 2;
  mg_info.n_smoothing[2] = 2;
  mg_info.n_smoothing[3] = 2;
  mg_info.n_smoothing[4] = 2;
  mg_info.n_smoothing[5] = 2;
  mg_info.n_smoothing[6] = 2;
  mg_info.n_smoothing[7] = 2;
  mg_info.n_smoothing[8] = 2;
  mg_info.n_smoothing[9] = 20;

  

  // Create the multigrid context
  MG_create(&mg, mg_info, physics, grid);

  // Set initial conditions
  MG_first_time(mg);

  // Do some cycles
  for ( i = 0 ; i < 20 ; ++i)
    {
      REAL linf;
      char const *names[] = {"u", "r"};
      struct DOFVector *vars[4];
      char fname[500];
      
      MG_cycle(mg, 0, NULL, mg->du[ZERO][0]);
      DOFVector_axby(1, mg->du[ZERO][0], 1, mg->u[ZERO][0], mg->u[ZERO][0]);
      
      MG_residual(mg, 0, mg->u[ZERO][0], mg->f[ZERO][0], mg->r[ZERO][0]);
      DOFVector_norms(mg->r[ZERO][0], NULL, NULL, &linf);
      printf("Iter %d: residual: %e \n", i, linf);

      sprintf(fname, "solution_%d.vtk", i);
      vars[0] = mg->u[ZERO][0];
      vars[1] = mg->r[ZERO][0];
      IO_write_vtk(fname, 2, vars, names);
    }


  // Free memory
  MG_destroy(&mg);
  Grid_destroy(&grid);

  return 0;
}

/*
  Local Variables:
  mode:c++
  c-file-style:"GNU"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
