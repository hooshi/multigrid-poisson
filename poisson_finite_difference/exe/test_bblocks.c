/** test_bblocks.c
 *  Some tests for the multigrid solver.
 *  Shayan Hoshyari
 *  October 2015
 */

#include <math.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

#include "defs.h"

//boundary conditions
static double _fzero(const double x, void* data)
{
  return 0;
} //left down and right

static double _fcospx(const double y, void* data)
{
  return cos(PI*y);
} //top

//analytical solution
// static double _fanalytic(const double x,const double y, void* data)
// {
//   return cos(PI*x)*sinh(PI*y)/sinh(PI);
// }

//source term
static double _fsource(const double x, const double y, void* data)
{
  return 0;
}

static struct Physics Physics_create_default()
{
  struct Physics physics;
  
   // Create the physics
  physics.boundary_south.type = BDRY_DIRICHLET;
  physics.boundary_east.type =  BDRY_NEUMANN;
  physics.boundary_north.type = BDRY_DIRICHLET;
  physics.boundary_west.type =  BDRY_NEUMANN;
  physics.boundary_south.value = _fzero;
  physics.boundary_east.value =  _fzero;
  physics.boundary_north.value = _fcospx;
  physics.boundary_west.value =  _fzero;
  physics.source_term = _fsource;

  return physics;
}

//
// Test smoothing, and transfer, don't do cycle
// 
int test_1(int argc, char *argv[])
{
  struct Grid *grid;
  struct Physics physics;
  struct MGInfo mg_info;
  struct MG *mg;
  int  i;

  // Create the Grid
  Grid_create(&grid, 1, 1, 10, 20);

  // physics
  physics = Physics_create_default();
 
  // Set the multigrid options
  mg_info = MGInfo_create(SMOOTHER_JACOBI, CYCLE_TYPE_V, 2);
  mg_info.relaxation_factor = 2./3;
  mg_info.atol = 1e-10;

  // Create the multigrid context
  MG_create(&mg, mg_info, physics, grid);

  // Set initial conditions
  MG_first_time(mg);

  // Set ghost cell values
  MG_explicit_ghost_eval(mg, 0, mg->u[ZERO][0]);

  // Print the ghost values
  IO_print_dof_vector(mg->u[0][0], stdout, TRUE);

  // Do one level of smoothing
  for ( i = 0 ; i < 500 ; ++i)
    {
      REAL du;
      du = MG_smooth_jacobi(mg, 0, mg->u[ZERO][0], mg->f[ZERO][0], mg->du[0][0]);
      printf("Iter %d: change: %e \n", i, du);
      DOFVector_axby(1, mg->du[ZERO][0], 1, mg->u[ZERO][0], mg->u[ZERO][0]); 
    }

  // Write the solution
  {
    char const * names[] = {"Solution"};
    IO_write_vtk("solution_0.vtk", 1, &mg->u[ZERO][0], names);
  }

  // Move solution to coarser grid
  MG_restrict_inject(mg, 0, 1, mg->u[ZERO][0], mg->u[0][1]);
  // Write the solution
  {
    char const * names[] = {"Solution"};
    IO_write_vtk("solution_1.vtk", 1, &mg->u[ZERO][1], names);
  }

  // Move solution back to fine grid -- inject
  MG_prolongate_inject(mg, 1, 0, mg->u[ZERO][1], mg->u[ONE][0]);
  // Write the solution
  {
    char const * names[] = {"Solution"};
    IO_write_vtk("solution_2.vtk", 1, &mg->u[ONE][0], names);
  }

  // Move solution back to fine grid -- interpolate
  MG_prolongate_interpolate(mg, 1, 0, mg->u[ZERO][1], mg->u[ONE][0]);
  // Write the solution
  {
    char const * names[] = {"Solution"};
    IO_write_vtk("solution_3.vtk", 1, &mg->u[ONE][0], names);
  }

  // Free memory
  MG_destroy(&mg);
  Grid_destroy(&grid);

  return 0;
}

//
// Actually !! Test cycle.
// 
int test_2(int argc, char *argv[])
{
  struct Grid *grid;
  struct Physics physics;
  struct MGInfo mg_info;
  struct MG *mg;
  int  i;

  // Create the Grid
  Grid_create(&grid, 1, 1, 10, 20);

  // physics
  physics = Physics_create_default();
 
  // Set the multigrid options
  mg_info = MGInfo_create(SMOOTHER_JACOBI, CYCLE_TYPE_V, 2);
  mg_info.relaxation_factor = 2./3;
  mg_info.atol = 1e-10;
  mg_info.n_smoothing[1] = 500;
  mg_info.n_smoothing[1] = 10;

  // Create the multigrid context
  MG_create(&mg, mg_info, physics, grid);

  // Set initial conditions
  MG_first_time(mg);

  // Do 10 level of smoothing
  DOFVector_set_zero(mg->u[ZERO][0]);
  for ( i = 0 ; i < 10 ; ++i)
    {
      REAL linf_du, linf_r;
      MG_smooth_jacobi(mg, 0, mg->u[ZERO][0], mg->f[ZERO][0], mg->du[ZERO][0]);    
      MG_residual     (mg, 0, mg->u[ZERO][0], mg->f[ZERO][0], mg->r[ZERO][0]);
      DOFVector_norms(mg->r[ZERO][0], NULL, NULL, &linf_r );
      DOFVector_norms(mg->du[ZERO][0], NULL, NULL, &linf_du );
      printf("Iter %d: change: %e  residual: %e\n", i, linf_du, linf_r);
      
      DOFVector_axby(1, mg->du[ZERO][0], 1, mg->u[ZERO][0], mg->u[ZERO][0]); 
    }

  {
    REAL linf_r0, linf_r1;
    char const *names[] = {"u", "f", "r"};
    struct DOFVector *vars[4];

    // Find the residual
    MG_residual(mg, 0, mg->u[ZERO][0], mg->f[ZERO][0], mg->r[ZERO][0]);
    DOFVector_norms(mg->r[ZERO][0], NULL, NULL, &linf_r0);
    vars[0]= mg->u[ZERO][0];
    vars[1]= mg->f[ZERO][0];
    vars[2]= mg->r[ZERO][0];
    IO_write_vtk("solution_0.vtk", 3, vars, names);
    
    // Move to residual to the grid
    MG_restrict_inject(mg, 0, 1, mg->r[ZERO][0], mg->f[ZERO][1]);
    
    // Find residual again
    DOFVector_set_zero(mg->u[ZERO][1]);
    MG_residual(mg, 1, mg->u[ZERO][1], mg->f[ZERO][1], mg->r[ZERO][1]);
    DOFVector_norms(mg->r[ZERO][1], NULL, NULL, &linf_r1);
    vars[0]= mg->u[ZERO][1];
    vars[1]= mg->f[ZERO][1];
    vars[2]= mg->r[ZERO][1];
    IO_write_vtk("solution_1.vtk", 3, vars, names);

    fprintf(stdout, "ResF:%e, ResC: %e\n", linf_r0, linf_r1);
  }

  // Do 100 smoothing on coarse grid
  // set source to zero
  // DOFVector_set_zero(mg->f[ZERO][1]);
  // DOFVector_set_zero(mg->u[ZERO][1]);
  for ( i = 0 ; i < 100; ++i)
    {
      REAL linf_du, linf_r;
      linf_du = MG_smooth_jacobi(mg, 1, mg->u[ZERO][1], mg->f[ZERO][1], mg->du[ZERO][1]);    
      // Find residual
      MG_residual(mg, 1, mg->u[ZERO][1], mg->f[ZERO][1], mg->r[ZERO][1]);
      DOFVector_norms(mg->r[ZERO][1], NULL, NULL, &linf_r);
      if(i%10==0) printf("\tIter %d: change: %e  residual: %e\n", i, linf_du, linf_r);
      
      DOFVector_axby(1, mg->du[ZERO][1], 1, mg->u[ZERO][1], mg->u[ZERO][1]); 
    }

  // Move the correction to the lower grid
  {
    REAL linf_r0, linf_r1;
    char const *names[] = {"u", "f", "r"};
    struct DOFVector *vars[4];

    // Find the residual on the coarse grid
    MG_residual(mg, 1, mg->u[ZERO][1], mg->f[ZERO][1], mg->r[ZERO][1]);
    DOFVector_norms(mg->r[ZERO][1], NULL, NULL, &linf_r1);
    vars[0]= mg->u[ZERO][1];
    vars[1]= mg->f[ZERO][1];
    vars[2]= mg->r[ZERO][1];
    IO_write_vtk("solution_2.vtk", 3, vars, names);
    printf("Residual coarse: %lf \n", linf_r1);

    // Move to residual to the fine grid
    MG_prolongate_interpolate(mg, 1, 0, mg->u[ZERO][1],  mg->du[ZERO][0]);

    // Add it to the solution and find the residual again
    DOFVector_axby(1, mg->du[ZERO][0], 1, mg->u[ZERO][0], mg->u[ZERO][0]);
    MG_residual(mg, 0, mg->u[ZERO][0], mg->f[ZERO][0], mg->r[ZERO][0]);
    DOFVector_norms(mg->r[ZERO][0], NULL, NULL, &linf_r0);
    vars[0]= mg->u[ZERO][0];
    vars[1]= mg->f[ZERO][0];
    vars[2]= mg->r[ZERO][0];
    IO_write_vtk("solution_3.vtk", 3, vars, names);
    fprintf(stdout, "ResF:%e\n", linf_r0);
  }

  // Do 10 level of smoothing on fine again
  for ( i = 0 ; i < 10 ; ++i)
    {
      REAL linf_du, linf_r;
      MG_smooth_jacobi(mg, 0, mg->u[ZERO][0], mg->f[ZERO][0], mg->du[ZERO][0]);    
      MG_residual     (mg, 0, mg->u[ZERO][0], mg->f[ZERO][0], mg->r[ZERO][0]);
      DOFVector_norms(mg->r[ZERO][0], NULL, NULL, &linf_r );
      DOFVector_norms(mg->du[ZERO][0], NULL, NULL, &linf_du );
      printf("Iter %d: change: %e  residual: %e\n", i, linf_du, linf_r);
      
      DOFVector_axby(1, mg->du[ZERO][0], 1, mg->u[ZERO][0], mg->u[ZERO][0]); 
    }
  // write the solution again
  {
    REAL linf_r0;
    char const *names[] = {"u", "f", "r"};
    struct DOFVector *vars[4];

    // Find the residual
    DOFVector_axby(1, mg->du[ZERO][0], 1, mg->u[ZERO][0], mg->u[ZERO][0]);
    MG_residual(mg, 0, mg->u[ZERO][0], mg->f[ZERO][0], mg->r[ZERO][0]);
    DOFVector_norms(mg->r[ZERO][0], NULL, NULL, &linf_r0);
    fprintf(stdout, "ResF:%e\n", linf_r0);
    vars[0]= mg->u[ZERO][0];
    vars[1]= mg->f[ZERO][0];
    vars[2]= mg->r[ZERO][0];
    IO_write_vtk("solution_4.vtk", 3, vars, names);
  }


  // Free memory
  MG_destroy(&mg);
  Grid_destroy(&grid);

  return 0;
}


int main(int argc, char *argv[])
{

  return test_2(argc, argv);

  return 0;
}

/*
  Local Variables:
  mode:c++
  c-file-style:"GNU"
  c-file-offsets:((innamespace . 0)(inline-open . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
