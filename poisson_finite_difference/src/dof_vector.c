#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <string.h>

#include "defs.h"

void   DOFVector_create(struct DOFVector** dv, struct Grid* g)
{
  int nxplusghost, nyplusghost;
  nxplusghost = g->n_cv_x + 2;
  nyplusghost = g->n_cv_y + 2;
  
  (*dv) = (struct DOFVector*)malloc(sizeof(struct DOFVector));
  (*dv)->grid = g;
  (*dv)->values = (REAL*)calloc(nxplusghost*nyplusghost, sizeof(REAL));
}

void   DOFVector_destroy(struct DOFVector** dv)
{
  free( (*dv)->values );
  free( (*dv) );
  (*dv) = (struct DOFVector*)NULL;
}

REAL DOFVector_get_value(struct DOFVector* dv, int ii, int jj)
{
  int nxplusghost, nyplusghost;
  nxplusghost = dv->grid->n_cv_x + 2;
  nyplusghost = dv->grid->n_cv_y + 2;
  assert( ii >= 0 );
  assert( ii < nxplusghost );
  assert( jj >= 0);
  assert( jj < nyplusghost );
  return dv->values[jj*nxplusghost + ii];
}

void DOFVector_set_value(struct DOFVector* dv, int ii, int jj, REAL v)
{
  int nxplusghost, nyplusghost;
  nxplusghost = dv->grid->n_cv_x + 2;
  nyplusghost = dv->grid->n_cv_y + 2;
  assert( ii >= 0 );
  assert( ii < nxplusghost );
  assert( jj >= 0);
  assert( jj < nyplusghost );
  
  dv->values[jj*nxplusghost + ii] = v;
}

void DOFVector_copy(struct DOFVector* from, struct DOFVector* to)
{
  int i,j;
  int nxplusghost, nyplusghost;
  REAL from_i_j;

  if( from->grid != to->grid ) EXIT();
  
  nxplusghost = from->grid->n_cv_x + 2;
  nyplusghost = from->grid->n_cv_y + 2;
  
  for(i=0; i<nxplusghost; ++i)
    {
      for(j=0; j<nyplusghost; ++j)
        {
          from_i_j = DOFVector_get_value(from, i, j);
          DOFVector_set_value(to, i, j, from_i_j);
        }
    }
}

void DOFVector_set_zero(struct DOFVector* dv)
{
  memset( dv->values, 0, sizeof(REAL)*(dv->grid->n_cv_x+2)*(dv->grid->n_cv_y+2) );
}


// c = a * x +  b * y;
void DOFVector_axby(REAL a, struct DOFVector* x, REAL b, struct DOFVector* y, struct DOFVector* z)
{
  int i,j;
  int nxplusghost, nyplusghost;
  REAL z_i_j, x_i_j, y_i_j;

  if( y && z->grid != y->grid ) EXIT();
  if( x && z->grid != x->grid ) EXIT();
  
  nxplusghost = z->grid->n_cv_x + 2;
  nyplusghost = z->grid->n_cv_y + 2;
  
  for(i=0; i<nxplusghost; ++i)
    {
      for(j=0; j<nyplusghost; ++j)
        {
          x_i_j = y_i_j = 0;
          if(y) y_i_j = DOFVector_get_value(y, i, j);
          if(x) x_i_j = DOFVector_get_value(x, i, j);
          z_i_j = a * x_i_j + b * y_i_j;
          DOFVector_set_value(z, i, j, z_i_j);
        }
    }
}

void DOFVector_norms(struct DOFVector* dv, REAL *__l1, REAL *__l2, REAL *__linf)
{
  int i,j, n_dof;
  int nxplusghost, nyplusghost;
  REAL v_i_j;
  REAL l1, l2, linf;
  
  nxplusghost = dv->grid->n_cv_x + 2;
  nyplusghost = dv->grid->n_cv_y + 2;
  linf = 0;
  l1 = 0;
  l2 = 0;
  n_dof = 0;
  
  for(i=1; i<nxplusghost-1; ++i)
    {
      for(j=1; j<nyplusghost-1; ++j)
        {
          v_i_j = DOFVector_get_value(dv, i, j);
          l1 += ABS(v_i_j);
          l2 += v_i_j*v_i_j;
          linf = MAX(linf, v_i_j);
          ++n_dof;
        }
    }
  l1 /= n_dof;
  l2 /= n_dof; l2 = sqrt(l2);

  // Return values
  if(__l1)   *__l1   = l1  ;
  if(__l2)   *__l2   = l2  ;
  if(__linf) *__linf = linf;
  
}

/*
  Local Variables:
  mode:c
  c-file-style:"GNU"
  c-file-offsets:((innamespace . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
