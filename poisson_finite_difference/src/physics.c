#include <stdlib.h>
#include <assert.h>

#include "defs.h"

struct Physics Physics_create(struct BoundaryEdge south,
                              struct BoundaryEdge east,
                              struct BoundaryEdge north,
                              struct BoundaryEdge west,
                              Function2D source_term)
{
    struct Physics physics;
    physics.boundary_south = south;
    physics.boundary_east = east;
    physics.boundary_north = north;
    physics.boundary_west = west;
    physics.source_term = source_term;

    return physics;
}

/*
  Local Variables:
  mode:c
  c-file-style:"GNU"
  c-file-offsets:((innamespace . 0)(case-label . +))
  indent-tabs-mode:nil
  fill-column:99
  End:
*/
