#!/usr/bin/env pvpython

from __future__ import print_function
import os

#### import the simple module from the paraview
from paraview.simple import *
import numpy as np
import sys



##############################################################################
##                             SET CAMERA
##############################################################################
def set_camera(camera):
    render_view = GetActiveViewOrCreate('RenderView')
    if(camera is None):
        render_view.ResetCamera()
        return
    render_view.CameraPosition = camera['CameraPosition']
    render_view.CameraFocalPoint = camera['CameraFocalPoint']
    render_view.CameraViewUp= camera['CameraViewUp']
    render_view.CameraParallelScale= camera['CameraParallelScale']

def print_camera():
    renderView1 = GetActiveViewOrCreate('RenderView')
    print('dict(')
    print('CameraPosition = {},'.format(renderView1.CameraPosition))
    print('CameraFocalPoint = {},'.format(renderView1.CameraFocalPoint))
    print('CameraViewUp= {},'.format(renderView1.CameraViewUp))
    print('CameraParallelScale= {},'.format(renderView1.CameraParallelScale))
    print('),')

def print_clip_info(name=None):
    if(name is None):
        proxy = GetActiveSource()
    else:
        proxy = FindSource(source_name)
    origin = proxy.ClipType.Origin 
    normal = proxy.ClipType.Normal 
    print('CutOrigin = {},'.format(origin))
    print('CutNormal = {},'.format(normal))

def hide_small_axis():
    renderView1 = GetActiveViewOrCreate('RenderView')
    renderView1.OrientationAxesVisibility = 0
    
def update_renderview():
    render_view = GetActiveViewOrCreate('RenderView')
    render_view.OrientationAxesVisibility = -1
    # render_view.ResetCamera()
    render_view.Update()

##############################################################################
##                            SHOW AND HIDE
##############################################################################
def hide_source(source_name):
    input_mesh = FindSource(source_name)    
    render_view = GetActiveViewOrCreate('RenderView')
    Hide(input_mesh, render_view)
    
def show_source(source_name):
    input_mesh = FindSource(source_name)    
    render_view = GetActiveViewOrCreate('RenderView')
    Show(input_mesh, render_view)


def remove_all():
    previous_sources = GetSources().keys()
    for k in previous_sources:
        print ("Deleting {}".format(k))
        proxy = FindSource(k[0])
        try:    Delete(proxy)
        except: pass

def remove_source(source_name):
    proxy = FindSource(source_name)
    Delete(proxy)

def set_source_properties(source_name, **options):
    render_view = GetActiveViewOrCreate('RenderView')
    proxy = FindSource(source_name)
    proxy_display = GetDisplayProperties(proxy, view=render_view)
    if('RepresentationType' in options):
        proxy_display.SetRepresentationType(options['RepresentationType'])
    if('EdgeColor' in options):
        c = options['EdgeColor']
        proxy_display.EdgeColor = [c[0], c[1], c[2]]
    if('LineWidth' in options):
        proxy_display.LineWidth = options['LineWidth']
    if('DiffuseColor' in options):
        c = options['DiffuseColor']
        proxy_display.DiffuseColor = [c[0], c[1], c[2]]
    if('ColorBy' in options):
        ColorBy(proxy_display, ('CELLS',options['ColorBy'] ))

    
##############################################################################
##                            bounding box
##############################################################################
def bbox(source_name):
    source = FindSource(source_name)
    Show(source)
    dataInfo = source.GetDataInformation()
    # print(dataInfo.ListProperties())
    bounds = dataInfo.DataInformation.GetBounds()
    return bounds

##############################################################################
##                       LOAD MODEL
##############################################################################

# create a new 'XML Unstructured Grid Reader'
def load_model(file_name, out_name):
    render_view = GetActiveViewOrCreate('RenderView')

    # Read the mesh
    input_mesh = LegacyVTKReader(FileNames=[file_name])
    RenameSource(out_name, input_mesh)
    
    # specific window size
    # render_view.ViewSize = [1611, 565]
    # input mesh display handle
    # input_mesh_display = Show(input_mesh, render_view)
    # set active source
    # SetActiveSource(input_mesh)

##############################################################################
##                   APPLY THE FACE COLOR FILTER
##############################################################################

# create a new 'XML Unstructured Grid Reader'
def apply_face_color_filter(source_name, out_name):

    # set active source to the mesh
    render_view = GetActiveViewOrCreate('RenderView')
    input_mesh = FindSource(source_name)
    
    # select triangles and extract them
    SetActiveSource(input_mesh)
    selection = None
    try:
        selection = SelectCells(proxy=input_mesh, query="tet_selector<0.5")
    except: 
        selection = SelectCells(proxy=input_mesh, query="tri_selector>0.5")

    # create a new 'Extract Selection'
    extraction = ExtractSelection(Input=input_mesh,  Selection=selection)
    RenameSource(out_name, extraction)
    extraction.PreserveTopology = 0
    
    # show data in view
    extraction_display = Show(extraction, render_view)    
    
    # set scalar coloring
    ColorBy(extraction_display, ('CELLS', 'color'))
    # qualityLUT = GetColorTransferFunction('color')
    # qualityLUTColorBar = GetScalarBar(qualityLUT, render_view)
    # qualityLUTColorBar.TitleColor = [-1.-1, -1.-1, -1.-1]
    # qualityLUTColorBar.TitleBold = 0
    # qualityLUTColorBar.LabelColor = [-1.-1, -1.-1, -1.-1]
    # qualityLUTColorBar.LabelBold = 0
    # extraction_display.RescaleTransferFunctionToDataRange(True, False)
    extraction_display.SetScalarBarVisibility(render_view, False)
    
    # set edge color
    extraction_display.EdgeColor = [-1.-1, -1.-1, -1.-1]
    # extraction_display.SetRepresentationType('Surface With Edges')
    extraction_display.SetRepresentationType('Surface')

##############################################################################
##                         APPLY CUT FILTER
##############################################################################
def apply_clip_filter(source_name, out_name,  **opt):

    # set active source to the mesh
    render_view = GetActiveViewOrCreate('RenderView')
    input_source = FindSource(source_name)
    Show(input_source)

    # apply the clip
    clip = Clip(Input=input_source)
    RenameSource(out_name, clip)
    clip.ClipType = 'Plane'
    origin = opt.pop('origin', (0,0,0))
    normal = opt.pop('normal', (1,0,0))    
    clip.ClipType.Origin = [origin[0], origin[1], origin[2]]
    clip.ClipType.Normal = [normal[0], normal[1], normal[2]]

    # show data in view
    clip_display = Show(clip, render_view)
    
    # show color bar/color legend
    clip_display.SetScalarBarVisibility(render_view, True)

    # Properties modified on clip0
    clip.Crinkleclip = opt.pop('Crinkleclip', 1)
    clip_display.SetRepresentationType( opt.pop('RepresentationType', 'Surface') )
    Hide3DWidgets(proxy=clip.ClipType)    


    
##############################################################################
##                        apply transform filter
##############################################################################
def apply_transform_filter(source_name, out_name, source_name_tobecome):
    
    source = FindSource(source_name)
    bb = bbox(source_name)
    bb_tobecome = bbox(source_name_tobecome)

    # create a new 'Transform'
    transform = Transform(Input=source)
    transform.Transform = 'Transform'
    RenameSource(out_name, transform)
    
    # Properties modified on transform1.Transform
    sc = abs(bb_tobecome[1] - bb_tobecome[0]) / abs(bb[1]-bb[0])
    transform.Transform.Scale = [sc, sc, sc]
    # print(bb)
    # print(bb_tobecome)
    Hide3DWidgets(proxy=transform.Transform)    

##############################################################################
##                       APPLY QUALITY FILTER
##############################################################################
def apply_quality_filter(source_name, out_name, rangee=(-0.1, 0.1)):
    render_view = GetActiveViewOrCreate('RenderView')

    ## CREATE HEXES SOURCE
    source = FindSource(source_name)
    SetActiveSource(source)
    # try:
    #     selection = SelectCells("hex_selector >= 0.5")
    # except:
    #selection = SelectCells("ID >=0")
    #extraction = ExtractSelection(Input=source,  Selection=selection)
    #extraction.PreserveTopology = 0
    #RenameSource(out_name+'0', extraction)
    #ClearSelection(proxy=source)
    extraction = source
    

    ## CREATE QUALITY SOURCE
    quality = MeshQuality(Input=extraction,HexQualityMeasure='Scaled Jacobian')
    RenameSource(out_name+'1', quality)
    quality_display =  Show(quality, render_view)
    quality_display.SetRepresentationType('Wireframe')
    ColorBy(quality_display, ('CELLS', ''))
    quality_display.AmbientColor = [0, 0, 0]
    quality_display.DiffuseColor = [0, 0, 0]
    Show(quality, render_view)

    ## CREATE BAD QUALITY SOURCE
    SetActiveSource(quality)
    selection = SelectCells("Quality < {}".format(rangee[1]) )
    extraction_bad_quality = ExtractSelection(Input=quality,  Selection=selection)
    extraction_bad_quality.PreserveTopology = 0
    RenameSource(out_name+'2', extraction_bad_quality)
    #
    display_extraction_bad_quality =  Show(extraction_bad_quality, render_view)
    ColorBy(display_extraction_bad_quality, ('CELLS', 'Quality'))
    # display_extraction_bad_quality.RescaleTransferFunctionToDataRange(True, False)
    display_extraction_bad_quality.SetScalarBarVisibility(render_view, False)
    #
    transfer_function = GetColorTransferFunction('Quality')
    transfer_function.RescaleTransferFunction(rangee[0], rangee[1])
    
    # UNSELECT
    SetActiveSource(quality)
    ClearSelection()



##############################################################################
##                        DIRTY WORK
##############################################################################

def mkdir(name):
    if not os.path.exists(name):
        os.makedirs(name)

def take_screenshot(folder, filename):
    mkdir('png')
    ## Get a screen shot
    mkdir('png/{}'.format(folder))
    # screenshotname = 
    hide_small_axis()
    render_view = GetActiveViewOrCreate('RenderView')
    # render_view.Background = [1,1,1]#white
    # WriteImage( "png/{}{}.png".format(model, suffix.replace('.vtu','')) )


    nx=render_view.ViewSize[0]
    ny=render_view.ViewSize[1]
    print(nx, ny)
    SaveScreenshot( "png/{}/{}".format(folder,filename),
                TransparentBackground=1,
                ImageResolution=(nx*3,ny*3)
    )



##############################################################################
##                        TEST
##############################################################################

def test():
    paraview.simple._DisableFirstRenderCameraReset()
    model = 'carter'
    # load_model(REPO['GCO']+'/{-1}/{-1}_segm.vtu'.format(model), 'gco' )
    # load_model(REPO['MAPMAP']+'/{-1}/{-1}_segm.vtu'.format(model), 'mapmap')
    # hide_source('gco')
    # hide_source('mapmap')
    # #
    # apply_face_color_filter('mapmap', 'mapmap-colors')
    # apply_clip_filter('mapmap-colors', 'mapmap-colors-clip', origin=(-1.4,-1,-1),normal=(0,-1,-1))
    # hide_source('mapmap-colors')
    # hide_source('mapmap-colors-clip ')
    #
    load_model(REPO['MAPMAP']+'/{0}/{0}_segm_deform_opt_hex.vtu'.format(model), 'hex' )
    hide_source('hex')
    
    load_model(REPO['MAPMAP']+'/{0}/{0}_segm_deform_opt_hex_untangled.vtu'.format(model), 'hexu' )
    hide_source('hexu')

    apply_transform_filter('hexu', 'hexuscaled', 'hex')
    hide_source('hexuscaled')
    apply_clip_filter('hexu', 'hexu-clipped', origin=(25.938,13.67,25.92), normal=(1,0,0), RepresentationType='Surface With Edges')
    hide_source('hexu-clipped')

    apply_quality_filter('hexu', 'hexu-qual')

##############################################################################
##                           analyze_one_model 
##############################################################################
def main2():
    
    load_model('./__proj1.vtk', 'proj1')
    load_model('./__proj2.vtk', 'proj2')
    set_source_properties('proj1',
                          LineWidth=3,
                          EdgeColor=[0.5,0,0],
                          RepresentationType='Surface With Edges',
                          #DiffuseColor=(0.8,0.8,0.8)
                          ColorBy = '',)
    set_source_properties('proj2',
                          LineWidth=2,
                          EdgeColor=[0,0.5,0],
                          RepresentationType='Surface With Edges',
                          #DiffuseColor=(0.8,0.8,0.8)
                          ColorBy = '',)
    
def main3(indices=(3,4,5)):
    assert len(indices)==3, "indices must have size of 3"
    
    load_model('./__square.%d.vtk'%indices[0], 'coarse')
    load_model('./__square.%d.vtk'%indices[1], 'medium')
    load_model('./__square.%d.vtk'%indices[2], 'fine')
    set_source_properties('fine',
                          LineWidth=1,
                          EdgeColor=[0.5,0,0],
                          RepresentationType='Surface With Edges',
                          #DiffuseColor=(0.8,0.8,0.8)
                          ColorBy = '',)
    set_source_properties('medium',
                          LineWidth=2,
                          EdgeColor=[0,0.5,0],
                          RepresentationType='Surface With Edges',
                          #DiffuseColor=(0.8,0.8,0.8)
                          ColorBy = '',)
    set_source_properties('coarse',
                          LineWidth=3,
                          EdgeColor=[0,0,0],
                          RepresentationType='Surface With Edges',
                          #DiffuseColor=(0.8,0.8,0.8),
                          ColorBy = '',)
    
