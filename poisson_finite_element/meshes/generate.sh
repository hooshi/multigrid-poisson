#!/usr/bin/env bash


A=0.4096;triangle -pqa${A}Djz square.poly
A=0.1024;triangle -prqa${A}Djz square.1
A=0.0512;triangle -prqa${A}Djz square.2
## Agressive
A=0.0016;triangle -prqa${A}Djz square.3
A=0.0001;triangle -prqa${A}Djz square.4
A=0.00000625; triangle -prqa${A}Djz square.5
A=0.000000390625; triangle -pqa${A}Djz square.6


mkdir -p dirt

mv square.[1567].* dirt/
#
cp precious/square.[234].* dirt/
#
rm -f square.[234].*


cp precious/square.msh dirt/


## For mg
cp square.poly squaremg.poly
A=0.00640000;triangle -pqa${A}Djz squaremg.poly
A=0.00160000;triangle -prqa${A}Djz squaremg.1
A=0.00040000;triangle -prqa${A}Djz squaremg.2
A=0.00010000;triangle -prqa${A}Djz squaremg.3
A=0.00002500;triangle -prqa${A}Djz squaremg.4
A=0.00000600;triangle -prqa${A}Djz squaremg.5
A=0.00000150;triangle -prqa${A}Djz squaremg.6
A=0.00000035;triangle -prqa${A}Djz squaremg.7

mv squaremg.[12345678].* dirt/
rm squaremg.poly
