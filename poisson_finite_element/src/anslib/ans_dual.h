#ifndef ANS_DUAL_H
#define ANS_DUAL_H

#include <assert.h>
#include <math.h>
#include <cstring>
#include <ostream>


template <int nVars>
class DualNumber
{
  double m_value;
  double m_deriv[nVars];
public:
  DualNumber(const double val, const int position) {
    m_value = val;
    for (int ii = 0; ii < nVars; ii++) {
      m_deriv[ii] = 0;
    }
    assert(position >= 0 && position < nVars);
    m_deriv[position] = 1;
  }
  // Promote a double (or other numeric type) to Dual w/o a derivative.
  DualNumber(const double val = 0) {
    m_value = val;
    memset(m_deriv, 0, nVars*sizeof(double));
//    for (int ii = 0; ii < nVars; ii++) {
//      m_deriv[ii] = 0;
//    }
  }

  // Accessors
  double operator()() const {return m_value;}
  double deriv(const int position) const {
    assert(position >= 0 && position < nVars);
    return m_deriv[position];
  }
  double& operator()() {return m_value;}
  double& deriv(const int position) {
    assert(position >= 0 && position < nVars);
    return m_deriv[position];
  }

  // Conversion to double
  // `explicit' only works in C++11 which  we do not support yet!
  // Also, westgrid systems have gcc4.4.7 which does not support c++11.
  // Of course, having such a conversion operator without the
  // `explicit' keyword is likely to create errors without the
  // developer even noticing.
  // explicit operator double() const {
  explicit operator double() const
  {
    return m_value;
  }

  // Addition operators
  DualNumber operator+(const DualNumber& a) const {
    DualNumber result(m_value + a.m_value);
    for (int ii = 0; ii < nVars; ii++) {
      result.m_deriv[ii] = m_deriv[ii] + a.m_deriv[ii];
    }
    return result;
  }
  DualNumber operator+(const double d) const {
    DualNumber result(*this);
    result.m_value += d;
    return result;
  }
  friend DualNumber operator+(const double d, const DualNumber& a) {
    DualNumber result(a);
    result.m_value += d;
    return result;
  }
  DualNumber operator+=(const DualNumber& a) {
    m_value += a.m_value;
    for (int ii = 0; ii < nVars; ii++) {
      m_deriv[ii] += a.m_deriv[ii];
    }
    return *this;
  }

  // Multiplication operators
  DualNumber operator*(const DualNumber& a) const {
    DualNumber result(m_value * a.m_value);
    for (int ii = 0; ii < nVars; ii++) {
      result.m_deriv[ii] = m_deriv[ii]*a.m_value + m_value*a.m_deriv[ii];
    }
    return result;
  }
  DualNumber operator*(const double d) const {
    DualNumber result(*this);
    result.m_value *= d;
    for (int ii = 0; ii < nVars; ii++) {
      result.m_deriv[ii] *= d;
    }
    return result;
  }
  friend DualNumber operator*(const double d, const DualNumber& a) {
    DualNumber result(a);
    result.m_value *= d;
    for (int ii = 0; ii < nVars; ii++) {
      result.m_deriv[ii] *= d;
    }
    return result;
  }
  DualNumber operator*=(const DualNumber& a) {
    for (int ii = 0; ii < nVars; ii++) {
      m_deriv[ii] = m_deriv[ii]*a.m_value + m_value*a.m_deriv[ii];
    }
    m_value = m_value * a.m_value;
    return *this;
  }

  // Subtraction operators
  DualNumber operator-(const DualNumber& a) const {
    DualNumber result(m_value - a.m_value);
    for (int ii = 0; ii < nVars; ii++) {
      result.m_deriv[ii] = m_deriv[ii] - a.m_deriv[ii];
    }
    return result;
  }
  DualNumber operator-(const double d) const {
    DualNumber result(*this);
    result.m_value -= d;
    return result;
  }
  friend DualNumber operator-(const double d, const DualNumber& a) {
    DualNumber result(-a);
    result.m_value += d;
    return result;
  }
  DualNumber operator-=(const DualNumber& a) {
    m_value -= a.m_value;
    for (int ii = 0; ii < nVars; ii++) {
      m_deriv[ii] -= a.m_deriv[ii];
    }
    return *this;
  }

  // Unary -
  DualNumber operator-() const {
    DualNumber result(0);
    result.m_value = -m_value;
    for (int ii = 0; ii < nVars; ii++) {
      result.m_deriv[ii] = -m_deriv[ii];
    }
    return result;
  }

  // Division operators
  DualNumber operator/(const DualNumber& a) const {
    DualNumber result(m_value / a.m_value);
    // Factored out the value of the divisor to save one division in
    // every loop.  Or at least, this way the compiler doesn't have to
    // find that optimization.
    for (int ii = 0; ii < nVars; ii++) {
      result.m_deriv[ii] = (-(m_value/a.m_value)*a.m_deriv[ii] + m_deriv[ii])
	/ a.m_value;
    }
    return result;
  }
  DualNumber operator/(const double d) const {
    DualNumber result(*this);
    result.m_value /= d;
    for (int ii = 0; ii < nVars; ii++) {
      result.m_deriv[ii] /= d;
    }
    return result;
  }
  friend DualNumber operator/(const double d, const DualNumber& a) {
    DualNumber result(d);
    result.m_value /= a.m_value;
    for (int ii = 0; ii < nVars; ii++) {
      result.m_deriv[ii] = - d / (a.m_value*a.m_value) * a.m_deriv[ii];
    }
    return result;
  }
  DualNumber operator/=(const DualNumber& a) {
    // Factored out the value of the divisor to save one division in
    // every loop.  Or at least, this way the compiler doesn't have to
    // find that optimization.
    for (int ii = 0; ii < nVars; ii++) {
      m_deriv[ii] = (-(m_value/a.m_value)*a.m_deriv[ii] + m_deriv[ii])
	/ a.m_value;
    }
    m_value = m_value / a.m_value;
    return *this;
  }

  bool operator==(const DualNumber& a) const {
    bool retVal = m_value == a.m_value;
    for (int ii = 0; ii < nVars && retVal; ii++) {
      retVal = m_deriv[ii] == a.m_deriv[ii];
    }
    return retVal;
  }

  // Some Rapsodia-like query functions.
   void set(const int pos, const double val){
 	  assert(pos >= 0 && pos < nVars);
 	  m_deriv[pos] = val;
   }
   double get(const int pos) const{
 	  assert(pos >= 0 && pos < nVars);
 	  return m_deriv[pos];
   }

   double const * toArray() const{
 	  return m_deriv ;
   }
   void toArray(const double *& array) const{
   	  array = m_deriv;
   	  //printf("reference version\n");
   }
   void toArray(double * array) const{
 	  for (int ii = 0 ; ii < nVars ; ii++){
 		  array[ii] = m_deriv[ii];
 	  }
 	  //printf("copy version\n");
   }

   // dual number = double
   // implicit constructor takes care of this.
//   DualNumber& operator=(const double val){
// 	  m_value = val;
// 	  for (int ii = 0; ii < nVars; ii++) {
// 		  m_deriv[ii] = 0;
// 	  }
// 	  return *this;
//   }
};

// output to stream
template<int nVars>
std::ostream& operator<< (std::ostream &strm, const DualNumber<nVars> a)
{
	strm << "val: " << a() << "\tderivs" << "(" << nVars << "): " ;
	for (int ii = 0; ii < nVars-1; ii++){
		strm << a.deriv(ii) << ", ";
	}
	strm << a.deriv(nVars-1);
    return strm;
}

// finite
template<int nVars>
int finite(const DualNumber<nVars>& a)
{
  const bool is_val_finite =  (bool)finite(a());

  bool are_derivs_finite = true;
  for (int var = 0 ; var < nVars ; var++)
  {
	  are_derivs_finite = are_derivs_finite && ( (bool)finite(a.deriv(var)) );
  }

  return are_derivs_finite && is_val_finite;
}

/***************************************************************************\
 *                      One Argument Function Overload
\***************************************************************************/
// name should be the name of a standard math library function
// deriv should be written as (math expression with "arg" as an argument)
//
// Example: d (tan(x)) / dx = 1 + tan(x)^2, so you'd use:
//    oneArgDeriv(tan, 1+tan(arg)*tan(arg))

#define oneArgDeriv(name, derivative) \
template <int nVars>					  \
inline DualNumber<nVars> name(const DualNumber<nVars> input) { \
    double arg = input();				  \
    DualNumber<nVars> result(name(arg));		  \
    double temp = (derivative);				  \
    for (int ii = 0; ii < nVars; ii++) {		  \
      result.deriv(ii) = temp*input.deriv(ii);		  \
    }							  \
    return result;					  \
  }

#define oneArgWithHelperDeriv(name, derivative)           \
  template <int nVars>                                    \
  DualNumber<nVars> name(const int intArg,                \
                         const DualNumber<nVars> input) { \
    double arg = input();                                 \
    DualNumber<nVars> result(name(intArg, arg));          \
    double temp = (derivative);                           \
    for (int ii = 0; ii < nVars; ii++) {                  \
      result.deriv(ii) = temp*input.deriv(ii);            \
    }                                                     \
    return result;                                        \
  }

/***************************************************************************\
 *                      Two Argument Function Overload
\***************************************************************************/
// The definition of a two argument function like:
// r = f(a, b)
// The derivatives are found from the formula:
// r' = (a' * r_a + b' * r_b)
//
// Syntax:
// funcName: Name of the function to overload.
// operation: f(a,b) which must be given in terms of av, bv.
// r_a, r_b: Partials derivatives of f divided by factor.
//           Must be given in terms of rv, av, bv.


#define twoArgDerivExtended(name, operation, r_a, r_b)                  \
template <int nVars>                                                    \
DualNumber<nVars> name(const DualNumber<nVars>& a,                      \
                       const DualNumber<nVars>& b) {                    \
    double av = a(), bv = b();                                          \
    double rv = (operation);                                            \
    double r_av = (r_a), r_bv = (r_b);                                  \
    DualNumber<nVars> r(rv);                                            \
    for (int ii = 0; ii < nVars; ii++) {                                \
        r.deriv(ii) = r_av * a.deriv(ii)+ r_bv * b.deriv(ii) ;          \
    }                                                                   \
    return r;                                                           \
}                                                                       \
template <int nVars>                                                    \
DualNumber<nVars> name(const DualNumber<nVars>& a, const double b) {    \
    double av = a(), bv = b;                                            \
    double rv = (operation);                                            \
    double r_av = (r_a);                                                \
    DualNumber<nVars> r(rv);                                            \
    for (int ii = 0; ii < nVars; ii++) {                                \
        r.deriv(ii) = r_av * a.deriv(ii);                               \
    }                                                                   \
    return r;                                                           \
}                                                                       \
template <int nVars>                                                    \
DualNumber<nVars> name(const double a, const DualNumber<nVars>& b) {    \
    double av = a, bv = b();                                            \
    double rv = (operation);                                            \
    double r_bv = (r_b);                                                \
    DualNumber<nVars> r(rv);                                            \
    for (int ii = 0; ii < nVars; ii++) {                                \
        r.deriv(ii) = r_bv * b.deriv(ii);                               \
    }                                                                   \
    return r;                                                           \
}

#define twoArgDeriv(funcName, r_a, r_b) \
        twoArgDerivExtended(funcName, funcName(av, bv), r_a, r_b)

// Trigonometric functions; all pre-C99

oneArgDeriv(sin, cos(arg))
oneArgDeriv(cos, -sin(arg))
oneArgDeriv(tan, 1+result()*result())
oneArgDeriv(asin, 1/sqrt(1-arg*arg))
oneArgDeriv(acos, -1/sqrt(1-arg*arg))
oneArgDeriv(atan, 1/(1+arg*arg))
twoArgDeriv(atan2, (-bv/sqrt(av*av + bv*bv)), (av/sqrt(av*av + bv*bv)) )

// Hyperbolic funcs

oneArgDeriv(sinh, cosh(arg))
oneArgDeriv(cosh, sinh(arg))
oneArgDeriv(tanh, 1-result()*result())

// Inverse hyperbolic funcs (C99)

oneArgDeriv(asinh, 1/(sqrt(arg*arg+1)))
oneArgDeriv(acosh, 1/(sqrt(arg*arg-1)))
oneArgDeriv(atanh, 1/(1-arg*arg))

// Exponential / log functions

oneArgDeriv(exp, result())
oneArgDeriv(log, 1/arg)
oneArgDeriv(log10, 1/(arg*M_LN10))

// Exponential / log functions (C99 and beyond)
// Missing: expm1, log1p

oneArgDeriv(exp10, result()*M_LN10) // GNU extension (?)
oneArgDeriv(exp2, result()*M_LN2)
oneArgDeriv(log2, 1./(arg*M_LN2))

// Power functions

oneArgDeriv(sqrt, 0.5/result())
twoArgDeriv(pow, (av<1e-7 ? bv*pow(av, bv-1) : bv*rv/av), log(av)*rv)


// Power functions, C99

twoArgDeriv(hypot, (av/rv), (bv/rv))
oneArgDeriv(cbrt, 1/(result()*result()*3))

// Absolute value

oneArgDeriv(fabs, (arg > 0 ? 1 : -1))

// Effectively non-differentiable, and so missing:
// fmax, fmin, fdim (fabs of diff), fmod (fp modulus), rounding, copysign, etc

// Missing: fma (fused mult-add)

// Missing: erf, erfc, lgamma, tgamma (C99)

// Missing: Bessel functions (X/Open extensions) j0, j1, jn, y0, y1, yn

/***************************************************************************\
 *                           Comparison Operators
\***************************************************************************/
#define cmpOperator(symbol)                                            \
template <int nVars>                                                   \
inline bool operator symbol(const DualNumber<nVars>& a,                       \
                     const DualNumber<nVars>& b){                      \
	return ( a() symbol b() );                                         \
}                                                                      \
template <int nVars>                                                   \
inline bool operator symbol(const DualNumber<nVars>& a,const double b){       \
	return ( a() symbol b );                                           \
}                                                                      \
template <int nVars>                                                   \
inline bool operator symbol(const double a, const DualNumber<nVars>& b){      \
	return ( a symbol b() );                                           \
}

cmpOperator(<)
cmpOperator(>)
cmpOperator(<=)
cmpOperator(>=)
cmpOperator(!=)
cmpOperator(==)


#endif /*ANS_DUAL_H*/
